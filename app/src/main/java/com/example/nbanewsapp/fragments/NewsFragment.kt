package com.example.nbanewsapp.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.widget.AppCompatImageButton
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nbanewsapp.R
import com.example.nbanewsapp.data.News
import com.example.nbanewsapp.databinding.FragmentNewsBinding
import com.example.nbanewsapp.mvp.NewsPresenter
import com.example.nbanewsapp.mvp.NewsPresenterViewInterface
import com.example.nbanewsapp.util.NewsAdapter
import com.squareup.picasso.Picasso
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class NewsFragment : MvpAppCompatFragment(), NewsPresenterViewInterface {
    private lateinit var binding: FragmentNewsBinding

    @InjectPresenter
    lateinit var mPresenter: NewsPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun fillNews(news: List<News>) {
        binding.recyclerNews.layoutManager = LinearLayoutManager(context)
        val adapter = NewsAdapter(news)
        adapter.setOnTouchCallback(object : NewsAdapter.Callback {
            override fun onClick(position: Int) {
                showNews(news[position])
            }
        })
        binding.recyclerNews.adapter = adapter
    }

    override fun serverError() = Toast.makeText(context, getString(R.string.msg_error_server), Toast.LENGTH_SHORT).show()

    @SuppressLint("InflateParams")
    private fun showNews(news: News) {
        val inflater = layoutInflater
        val popupView = inflater.inflate(R.layout.popup_news, null)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT

        val popupWindow = PopupWindow(popupView, width, height, false)

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0, 0)
        popupView.findViewById<AppCompatImageButton>(R.id.imageButton_popup_news_close)
            .setOnClickListener { popupWindow.dismiss() }

        popupView.setOnClickListener { popupWindow.dismiss() }

        popupView.findViewById<TextView>(R.id.text_popup_header).text = news.header
        popupView.findViewById<TextView>(R.id.text_popup_news_date).text = news.data
        popupView.findViewById<TextView>(R.id.text_popup_news_text).text = news.text
        if(news.img.length > 1) Picasso.get().load(news.img).fit().into(popupView.findViewById<ImageView>(R.id.image_popup_news_image))

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = requireActivity().getSystemService(Context.WINDOW_SERVICE) as WindowManager

        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }


}