package com.example.nbanewsapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nbanewsapp.R
import com.example.nbanewsapp.data.StandingsPlace
import com.example.nbanewsapp.databinding.FragmentStandingsBinding
import com.example.nbanewsapp.mvp.StandingsPresenter
import com.example.nbanewsapp.mvp.StandingsPresenterViewInterface
import com.example.nbanewsapp.util.StandingsAdapter
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class StandingsFragment : MvpAppCompatFragment(), StandingsPresenterViewInterface {
    private lateinit var binding: FragmentStandingsBinding

    @InjectPresenter
    lateinit var mPresenter: StandingsPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStandingsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun fillStandings(standings: List<StandingsPlace>) {
        binding.recyclerStandings.layoutManager = LinearLayoutManager(context)
        binding.recyclerStandings.adapter = StandingsAdapter(standings)
    }

    override fun serverError() = Toast.makeText(context, getString(R.string.msg_error_server), Toast.LENGTH_SHORT).show()

}