package com.example.nbanewsapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nbanewsapp.R
import com.example.nbanewsapp.data.Fact
import com.example.nbanewsapp.databinding.FragmentFactsBinding
import com.example.nbanewsapp.mvp.FactsPresenter
import com.example.nbanewsapp.mvp.FactsPresenterViewInterface
import com.example.nbanewsapp.util.FactsAdapter
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class FactsFragment : MvpAppCompatFragment(), FactsPresenterViewInterface {
    private lateinit var binding: FragmentFactsBinding

    @InjectPresenter
    lateinit var mPresenter: FactsPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFactsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun fillFacts(facts: List<Fact>) {
        binding.recyclerFacts.layoutManager = LinearLayoutManager(context)
        binding.recyclerFacts.adapter = FactsAdapter(facts)
    }

    override fun serverError() = Toast.makeText(context, getString(R.string.msg_error_server), Toast.LENGTH_SHORT).show()

}