package com.example.nbanewsapp.mvp

import com.example.nbanewsapp.data.StandingsPlace
import com.example.nbanewsapp.util.ServerClient
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@InjectViewState
class StandingsPresenter: MvpPresenter<StandingsPresenterViewInterface>() {

    override fun onFirstViewAttach() {
        ServerClient.create().getStandings().enqueue(object : Callback<List<StandingsPlace>> {
            override fun onResponse(call: Call<List<StandingsPlace>>, response: Response<List<StandingsPlace>>) {
                if(response.isSuccessful) {
                    if(response.body() != null) response.body()?.let {
                        it.toMutableList().sortWith { p0, p1 -> p0.place.compareTo(p1.place) }
                        viewState.fillStandings(it)
                    }
                    else viewState.serverError()
                } else viewState.serverError()
            }

            override fun onFailure(call: Call<List<StandingsPlace>>, t: Throwable) {
                viewState.serverError()
            }
        })
    }
}