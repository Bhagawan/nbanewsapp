package com.example.nbanewsapp.mvp

import com.example.nbanewsapp.data.Fact
import com.example.nbanewsapp.util.ServerClient
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class FactsPresenter: MvpPresenter<FactsPresenterViewInterface>() {

    override fun onFirstViewAttach() {
        ServerClient.create().getFacts().enqueue(object : Callback<List<Fact>> {
            override fun onResponse(call: Call<List<Fact>>, response: Response<List<Fact>>) {
                if(response.isSuccessful) {
                    if(response.body() != null) response.body()?.let { viewState.fillFacts(it) }
                    else viewState.serverError()
                } else viewState.serverError()
            }

            override fun onFailure(call: Call<List<Fact>>, t: Throwable) {
                viewState.serverError()
            }
        })
    }
}