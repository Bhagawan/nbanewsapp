package com.example.nbanewsapp.mvp

import com.example.nbanewsapp.data.Fact
import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface FactsPresenterViewInterface: MvpView {

    @SingleState
    fun fillFacts(facts: List<Fact>)

    @OneExecution
    fun serverError()
}