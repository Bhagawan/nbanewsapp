package com.example.nbanewsapp.mvp

import com.example.nbanewsapp.data.News
import com.example.nbanewsapp.util.ServerClient
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class NewsPresenter: MvpPresenter<NewsPresenterViewInterface>() {

    override fun onFirstViewAttach() {
        ServerClient.create().getNews().enqueue(object : Callback<List<News>> {
            override fun onResponse(call: Call<List<News>>, response: Response<List<News>>) {
                if(response.isSuccessful) {
                    if(response.body() != null) response.body()?.let { viewState.fillNews(it) }
                    else viewState.serverError()
                } else viewState.serverError()
            }

            override fun onFailure(call: Call<List<News>>, t: Throwable) {
                viewState.serverError()
            }
        })
    }
}