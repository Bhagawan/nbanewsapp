package com.example.nbanewsapp.mvp

import com.example.nbanewsapp.data.News
import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface NewsPresenterViewInterface: MvpView {

    @SingleState
    fun fillNews(news: List<News>)

    @OneExecution
    fun serverError()
}