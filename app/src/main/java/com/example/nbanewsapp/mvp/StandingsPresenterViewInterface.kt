package com.example.nbanewsapp.mvp

import com.example.nbanewsapp.data.StandingsPlace
import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface StandingsPresenterViewInterface: MvpView {

    @SingleState
    fun fillStandings(standings: List<StandingsPlace>)

    @OneExecution
    fun serverError()
}