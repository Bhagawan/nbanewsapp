package com.example.nbanewsapp.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class StandingsPlace(@SerializedName("place") val place: Int,
                          val name: String,
                          val logo: String,
                          val games: Int,
                          val won: Int,
                          val lost: Int,
                          val balls: String,
                          val points: Int)
