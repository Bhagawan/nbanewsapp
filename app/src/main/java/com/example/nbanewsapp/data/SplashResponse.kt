package com.example.nbanewsapp.data

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)

