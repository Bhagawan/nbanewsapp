package com.example.nbanewsapp.data

import androidx.annotation.Keep

@Keep
data class News(val header: String, val data: String, val img: String, val text: String )
