package com.example.nbanewsapp.data

import androidx.annotation.Keep

@Keep
data class Fact(val text: String)
