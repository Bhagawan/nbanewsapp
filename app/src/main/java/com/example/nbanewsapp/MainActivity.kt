package com.example.nbanewsapp

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.example.nbanewsapp.databinding.ActivityMainBinding
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var target: Target

    override fun onBackPressed() {
        val navController: NavController = Navigation.findNavController(this, R.id.fragmentContainerView)
        if(navController.backQueue.size > 2) {
            navController.popBackStack()
        } else finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loadBackground()
        val navController: NavController = Navigation.findNavController(this, R.id.fragmentContainerView)
        binding.bottomNavigationView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.newsFragment -> {
                    binding.textHeader.text = getString(R.string.header_news)
                    binding.textHeader.textSize = 34.0f
                }
                R.id.standingsFragment -> {
                    binding.textHeader.text = getString(R.string.header_standings)
                    binding.textHeader.textSize = 30.0f
                }
                R.id.factsFragment -> {
                    binding.textHeader.text = getString(R.string.header_facts)
                    binding.textHeader.textSize = 26.0f
                }
            }
        }
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                binding.layoutMain.background = BitmapDrawable(resources, bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/NBANewsApp/back.png").into(target)
        Picasso.get().load("http://195.201.125.8/NBANewsApp/logo.png").fit().into(binding.imageMainLogo)
    }
}