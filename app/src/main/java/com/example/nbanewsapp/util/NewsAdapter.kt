package com.example.nbanewsapp.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nbanewsapp.R
import com.example.nbanewsapp.data.News
import com.squareup.picasso.Picasso

class NewsAdapter  (private val items : List<News>) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    lateinit var callback: Callback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.header.text = items[position].header
        holder.date.text = items[position].data
        holder.itemView.setOnClickListener { callback.onClick(position) }
        if(items[position].img.length > 1) Picasso.get().load(items[position].img).into(holder.img)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val header: TextView = itemView.findViewById(R.id.text_item_news_header)
        val date: TextView = itemView.findViewById(R.id.text_item_news_date)
        val img: ImageView = itemView.findViewById(R.id.image_item_news_img)
    }

    fun setOnTouchCallback(callback :Callback) {
        this.callback = callback
    }

    interface Callback {
        fun onClick(position :Int)
    }
}