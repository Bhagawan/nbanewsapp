package com.example.nbanewsapp.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nbanewsapp.R
import com.example.nbanewsapp.data.StandingsPlace
import com.squareup.picasso.Picasso

class StandingsAdapter (private val items : List<StandingsPlace>) : RecyclerView.Adapter<StandingsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_standings, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(position == 0) {
            holder.place.text = ""
            holder.name.text = holder.itemView.context.getString(R.string.standings_name)
            holder.logo.setImageBitmap(null)
            holder.games.text = holder.itemView.context.getString(R.string.standings_games)
            holder.won.text = holder.itemView.context.getString(R.string.standings_won)
            holder.lost.text = holder.itemView.context.getString(R.string.standings_lost)
            holder.balls.text = holder.itemView.context.getString(R.string.standings_balls)
            holder.points.text = holder.itemView.context.getString(R.string.standings_points)
        } else {
            holder.place.text = items[position - 1].place.toString()
            holder.name.text = items[position - 1].name
            Picasso.get().load(items[position - 1].logo).resize(50,50).into(holder.logo)
            holder.games.text = items[position - 1].games.toString()
            holder.won.text = items[position - 1].won.toString()
            holder.lost.text = items[position - 1].lost.toString()
            holder.balls.text = items[position - 1].balls
            holder.points.text = items[position - 1].points.toString()
        }
    }

    override fun getItemCount(): Int {
        return if(items.isNotEmpty()) items.size + 1
        else 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val place: TextView = itemView.findViewById(R.id.text_item_standings_place)
        val name: TextView = itemView.findViewById(R.id.text_item_standings_name)
        val logo: ImageView = itemView.findViewById(R.id.image_item_standings_logo)
        val games: TextView = itemView.findViewById(R.id.text_item_standings_games)
        val won: TextView = itemView.findViewById(R.id.text_item_standings_won)
        val lost: TextView = itemView.findViewById(R.id.text_item_standings_lost)
        val balls: TextView = itemView.findViewById(R.id.text_item_standings_balls)
        val points: TextView = itemView.findViewById(R.id.text_item_standings_points)
    }
}