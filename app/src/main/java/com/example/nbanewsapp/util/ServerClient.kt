package com.example.nbanewsapp.util

import com.example.nbanewsapp.data.Fact
import com.example.nbanewsapp.data.News
import com.example.nbanewsapp.data.SplashResponse
import com.example.nbanewsapp.data.StandingsPlace
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ServerClient {

    @FormUrlEncoded
    @POST("NBANewsApp/splash.php")
    fun getSplash(@Field("locale") locale: String): Call<SplashResponse>

    @GET("NBANewsApp/news.json")
    fun getNews(): Call<List<News>>

    @GET("NBANewsApp/standings.json")
    fun getStandings(): Call<List<StandingsPlace>>

    @GET("NBANewsApp/facts.json")
    fun getFacts(): Call<List<Fact>>

    companion object {
        fun create() : ServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClient::class.java)
        }
    }

}